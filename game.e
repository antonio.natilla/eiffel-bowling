note
	description: "Summary description for {GAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME

create
	init

feature
   	frames : ARRAY2[INTEGER]
   	frame_counter : INTEGER
   	is_first_roll : BOOLEAN

   	init
   		do
   			frame_counter := 0
   			is_first_roll := true
   			create frames.make_filled (0, 12, 2)
  		end

	roll(n : INTEGER)
		do
			if frame_counter > 9 and frame_counter < 11 then
				frames.put(n, frame_counter, 0)
				frame_counter := frame_counter + 1
			else
				if is_first_roll = true then
					frames.put (n, frame_counter, 0)
					if n = 10 then
						frame_counter := frame_counter + 1
					else
						is_first_roll := false
					end
				else
					frames.put (n, frame_counter, 1)
					frame_counter := frame_counter + 1
					is_first_roll := true
				end
			end
		end


	score : INTEGER
	    local 	s : INTEGER
	    		i : INTEGER
		do
			from i := 0 until i < 10
			loop
                s := s + frames.item (i,0)
                if frames.item (i,0) = 10 then
                	if i < 9 then
                		s := s + frames.item (i+1, 0)
                		if frames.item (i+1,0) = 10 then
                			s := s + frames.item (i+2, 0)
                		else
                			s := s + frames.item (i+1, 1)
                		end
                	else
                		s := s + frames.item (i+1, 0) + frames.item (i+2, 0)
                	end
				else
					s := s + frames.item (i, 1)
					if i < 9 then
						if frames.item (i, 1) + frames.item (i, 0) = 10 then
							s := s + frames.item (i+1, 0)
						end
					else
						s := s + frames.item (i+1, 0)
					end
                end

				i := i+1
			end

			frame_counter := 0
			Result := s
		end


end
