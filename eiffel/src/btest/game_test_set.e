note
	description: "Summary description for {GAME_TEST_SET}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME_TEST_SET
inherit
	EQA_TEST_SET

redefine
	on_prepare
	end

feature

	g : GAME

	on_prepare
		do
			create g.init
		end

	rollmany(n, score : INTEGER)
		local i : INTEGER
		do
			from i := 0
			until i < n
			loop
				g.roll(score)
			end
		end

	testguttergame
		do
			rollmany(20, 0)
			assert("guttergame", g.score = 0)
		end

	testallones
		do
			rollmany(20, 1)
			assert("allones", g.score = 20)
		end

	testonestrike
		do
			g.roll(10)
			g.roll (3)
			g.roll (4)
			rollmany(16, 0)
			assert("onestrike", g.score = 24)
		end
end
